<!DOCTYPE html>
<html>
<head>
    <script>
        function showUser(str)
        {
            if (str=="")
            {
                document.getElementById("txtHint").innerHTML="";
                return;
            }
            if (window.XMLHttpRequest)
            {
                xmlhttp=new XMLHttpRequest();
            } else {
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function()
            {
                if (this.readyState==4 && this.status==200)
                {
                    document.getElementById("txtHint").innerHTML=this.responseText;
                }
            }
            xmlhttp.open("GET","getuser.php?q="+str,true);
            xmlhttp.send();
        }
    </script>
</head>
<body>
<form>
    <select name="users" onchange="showUser(this.value)">
        <option value="">Select one person:</option>
        <option value="1">Nipa</option>
        <option value="2">Mita</option>
        <option value="3">Shima</option>
        <option value="4">Dipa</option>
    </select>
</form>
<br>
<div id="txtHint"><b>Person info will be listed here.</b></div>
</body>
</html>